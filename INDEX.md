# more

Displays the contents of a text file one page at a time


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## MORE.LSM

<table>
<tr><td>title</td><td>more</td></tr>
<tr><td>version</td><td>4.3a</td></tr>
<tr><td>entered&nbsp;date</td><td>2007-10-01</td></tr>
<tr><td>description</td><td>Displays the contents of a text file one page at a time</td></tr>
<tr><td>keywords</td><td>freedos, more</td></tr>
<tr><td>author</td><td>Jim Hall &lt;jhall@freedos.org&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Imre &lt;imre.leber@telenet.be&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://github.com/FDOS/more</td></tr>
<tr><td>platforms</td><td>DOS  *requires KITTEN</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/More</td></tr>
</table>
